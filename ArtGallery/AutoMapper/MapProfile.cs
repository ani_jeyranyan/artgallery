﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using ArtGalleryDAL.Entities;
using ArtGalleryDTO.EntitiesDTO;


namespace AutoMapperLib
{
    public static class AutoMapper
    {
        public static IServiceCollection ConfigureAutomapper(this IServiceCollection services)
        {
            services.AddAutoMapper(typeof(MapProfile));
            return services;
        }
    }
    public class MapProfile : Profile
    {
        public MapProfile()
        {
            CreateMap<Buyer, BuyerDTO>()
                .ReverseMap();

            CreateMap<Seller, SellerDTO>()
            .ReverseMap();

            CreateMap<ShoppingCart, ShoppingCartDTO>()
                .ReverseMap()
                .ForPath(s => s.buyer, src => src.Ignore())
                .ForPath(s => s.artObject, src => src.Ignore());

            CreateMap<ArtObject, ArtObjectDTO>()
              .ReverseMap()
              .ForPath(s => s.seller, opt => opt.Ignore());

        }
    }
}

