﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ArtGalleryVIEW
{
    public partial class SignIn : Form
    {
        private Welcome welcome;
        private BuyerProfile buyer;
        private addObjextBtn seller;

        public SignIn()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void showPasswordCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (showPasswordCheckBox.Checked)
            {
                passwordTxtBox.UseSystemPasswordChar = false;
            }
            else
            {
                passwordTxtBox.UseSystemPasswordChar = true;
            }
        }

        private void passwordTxtBox_TextChanged(object sender, EventArgs e)
        {

        }


        private void goBackBtn_Click(object sender, EventArgs e)
        {
            if (welcome == null)
            {
                welcome = new Welcome();
            }
            welcome.Show();
            this.Hide();
        }

        private void signInBtn_Click(object sender, EventArgs e)
        {
            if(RoleComboBox.SelectedItem == null)
            {
                MessageBox.Show("Please select the role :) ");
            }
            else  if(RoleComboBox.SelectedItem.ToString() == "Buyer")
            {
                if(buyer == null)
                {
                    buyer = new BuyerProfile();
                }

                buyer.Show();
                this.Hide();
            }
            else if (RoleComboBox.SelectedItem.ToString() == "Seller")
            {
                if (seller == null)
                {
                    seller = new addObjextBtn();
                }

                seller.Show();
                this.Hide();
            }

        }

        private void Role_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
