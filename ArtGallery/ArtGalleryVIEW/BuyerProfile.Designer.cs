﻿namespace ArtGalleryVIEW
{
    partial class BuyerProfile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BuyerProfile));
            this.Logo = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.userProfileNameLbl = new System.Windows.Forms.Label();
            this.userNameLbl = new System.Windows.Forms.Label();
            this.signOutBtn = new System.Windows.Forms.Button();
            this.paymentBtn = new System.Windows.Forms.Button();
            this.artObjectBtn = new System.Windows.Forms.Button();
            this.cartBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.quitBtn = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // Logo
            // 
            this.Logo.AllowDrop = true;
            this.Logo.AutoEllipsis = true;
            this.Logo.AutoSize = true;
            this.Logo.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Logo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Logo.Font = new System.Drawing.Font("Segoe Script", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Logo.Location = new System.Drawing.Point(1115, 91);
            this.Logo.Name = "Logo";
            this.Logo.Size = new System.Drawing.Size(152, 30);
            this.Logo.TabIndex = 43;
            this.Logo.Text = "ART GALLERY";
            this.Logo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Silver;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(276, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1029, 26);
            this.panel2.TabIndex = 41;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Gray;
            this.panel1.Controls.Add(this.userProfileNameLbl);
            this.panel1.Controls.Add(this.userNameLbl);
            this.panel1.Controls.Add(this.signOutBtn);
            this.panel1.Controls.Add(this.paymentBtn);
            this.panel1.Controls.Add(this.artObjectBtn);
            this.panel1.Controls.Add(this.cartBtn);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(276, 759);
            this.panel1.TabIndex = 40;
            // 
            // userProfileNameLbl
            // 
            this.userProfileNameLbl.Location = new System.Drawing.Point(66, 23);
            this.userProfileNameLbl.Name = "userProfileNameLbl";
            this.userProfileNameLbl.Size = new System.Drawing.Size(198, 28);
            this.userProfileNameLbl.TabIndex = 62;
            // 
            // userNameLbl
            // 
            this.userNameLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.userNameLbl.Image = ((System.Drawing.Image)(resources.GetObject("userNameLbl.Image")));
            this.userNameLbl.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.userNameLbl.Location = new System.Drawing.Point(11, 9);
            this.userNameLbl.Name = "userNameLbl";
            this.userNameLbl.Size = new System.Drawing.Size(253, 54);
            this.userNameLbl.TabIndex = 62;
            this.userNameLbl.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.userNameLbl.UseMnemonic = false;
            // 
            // signOutBtn
            // 
            this.signOutBtn.BackColor = System.Drawing.Color.Transparent;
            this.signOutBtn.FlatAppearance.BorderSize = 0;
            this.signOutBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.signOutBtn.Font = new System.Drawing.Font("Segoe Script", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.signOutBtn.Image = ((System.Drawing.Image)(resources.GetObject("signOutBtn.Image")));
            this.signOutBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.signOutBtn.Location = new System.Drawing.Point(40, 672);
            this.signOutBtn.Name = "signOutBtn";
            this.signOutBtn.Size = new System.Drawing.Size(180, 66);
            this.signOutBtn.TabIndex = 64;
            this.signOutBtn.Text = "Sign Out";
            this.signOutBtn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.signOutBtn.UseMnemonic = false;
            this.signOutBtn.UseVisualStyleBackColor = false;
            this.signOutBtn.Click += new System.EventHandler(this.signOutBtn_Click);
            // 
            // paymentBtn
            // 
            this.paymentBtn.BackColor = System.Drawing.Color.Transparent;
            this.paymentBtn.FlatAppearance.BorderSize = 0;
            this.paymentBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.paymentBtn.Font = new System.Drawing.Font("Segoe Script", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.paymentBtn.Image = ((System.Drawing.Image)(resources.GetObject("paymentBtn.Image")));
            this.paymentBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.paymentBtn.Location = new System.Drawing.Point(21, 416);
            this.paymentBtn.Name = "paymentBtn";
            this.paymentBtn.Size = new System.Drawing.Size(235, 66);
            this.paymentBtn.TabIndex = 64;
            this.paymentBtn.Text = "Payments";
            this.paymentBtn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.paymentBtn.UseMnemonic = false;
            this.paymentBtn.UseVisualStyleBackColor = false;
            // 
            // artObjectBtn
            // 
            this.artObjectBtn.BackColor = System.Drawing.Color.Transparent;
            this.artObjectBtn.FlatAppearance.BorderSize = 0;
            this.artObjectBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.artObjectBtn.Font = new System.Drawing.Font("Segoe Script", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.artObjectBtn.Image = ((System.Drawing.Image)(resources.GetObject("artObjectBtn.Image")));
            this.artObjectBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.artObjectBtn.Location = new System.Drawing.Point(21, 310);
            this.artObjectBtn.Name = "artObjectBtn";
            this.artObjectBtn.Size = new System.Drawing.Size(235, 66);
            this.artObjectBtn.TabIndex = 63;
            this.artObjectBtn.Text = "Art Market";
            this.artObjectBtn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.artObjectBtn.UseMnemonic = false;
            this.artObjectBtn.UseVisualStyleBackColor = false;
            // 
            // cartBtn
            // 
            this.cartBtn.BackColor = System.Drawing.Color.Transparent;
            this.cartBtn.FlatAppearance.BorderSize = 0;
            this.cartBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cartBtn.Font = new System.Drawing.Font("Segoe Script", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cartBtn.Image = ((System.Drawing.Image)(resources.GetObject("cartBtn.Image")));
            this.cartBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cartBtn.Location = new System.Drawing.Point(21, 210);
            this.cartBtn.Name = "cartBtn";
            this.cartBtn.Size = new System.Drawing.Size(235, 66);
            this.cartBtn.TabIndex = 64;
            this.cartBtn.Text = "Shopping Cart";
            this.cartBtn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cartBtn.UseMnemonic = false;
            this.cartBtn.UseVisualStyleBackColor = false;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.Font = new System.Drawing.Font("Segoe Script", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(1135, 112);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 40);
            this.label1.TabIndex = 44;
            this.label1.Text = "Online Shop";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(6, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(101, 112);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Silver;
            this.panel3.Controls.Add(this.pictureBox1);
            this.panel3.Location = new System.Drawing.Point(1133, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(114, 117);
            this.panel3.TabIndex = 42;
            // 
            // quitBtn
            // 
            this.quitBtn.BackColor = System.Drawing.Color.Silver;
            this.quitBtn.FlatAppearance.BorderSize = 0;
            this.quitBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.quitBtn.Font = new System.Drawing.Font("Segoe Script", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.quitBtn.Image = ((System.Drawing.Image)(resources.GetObject("quitBtn.Image")));
            this.quitBtn.Location = new System.Drawing.Point(1254, 9);
            this.quitBtn.Name = "quitBtn";
            this.quitBtn.Size = new System.Drawing.Size(41, 31);
            this.quitBtn.TabIndex = 61;
            this.quitBtn.UseMnemonic = false;
            this.quitBtn.UseVisualStyleBackColor = false;
            this.quitBtn.Click += new System.EventHandler(this.quitBtn_Click);
            // 
            // BuyerProfile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1305, 759);
            this.Controls.Add(this.quitBtn);
            this.Controls.Add(this.Logo);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "BuyerProfile";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "BuyerProfile";
            this.Load += new System.EventHandler(this.BuyerProfile_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label Logo;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button quitBtn;
        private System.Windows.Forms.Button artObjectBtn;
        private System.Windows.Forms.Button cartBtn;
        private System.Windows.Forms.Button paymentBtn;
        private System.Windows.Forms.Button signOutBtn;
        private System.Windows.Forms.Label userNameLbl;
        private System.Windows.Forms.Label userProfileNameLbl;
    }
}