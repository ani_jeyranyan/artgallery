﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ArtGalleryVIEW
{
    public partial class Welcome : Form
    {
        private SignIn signIn;
        private Register register;

        public Welcome()
        {
            InitializeComponent();
        }


        private void Welcome_Load(object sender, EventArgs e)
        {

        }

        private void signInBtn_Click(object sender, EventArgs e)
        {
            if (signIn == null)
            {
                signIn = new SignIn();
            }
            signIn.Show();
            this.Hide();
        }

        private void registerBtn_Click(object sender, EventArgs e)
        {
            if(register == null)
            {
                register = new Register();
            }
            register.Show();
            this.Hide();
        }

        private void quitBtn_Click_1(object sender, EventArgs e)
        {
            DialogResult dialog = new DialogResult();

            dialog = MessageBox.Show("Do you want to close the application?  :(", "Alert", MessageBoxButtons.YesNo);

            if (dialog == DialogResult.Yes)
            {
                System.Environment.Exit(1);
            }
        }
    }
}
