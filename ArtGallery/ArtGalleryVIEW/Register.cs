﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;

namespace ArtGalleryVIEW
{
    public partial class Register : Form
    {
        private Welcome welcome;

        public Register()
        {
            InitializeComponent();
        }

        private void accountLbl_Click(object sender, EventArgs e)
        {

        }

        private void passwordLbl_Click(object sender, EventArgs e)
        {

        }

        private void goBackBtn_Click(object sender, EventArgs e)
        {
            if (welcome == null)
            {
                welcome = new Welcome();
            }
            welcome.Show();
            this.Hide();
        }

        private void showPasswordCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (showPasswordCheckBox.Checked)
            {
                passTxtBox.UseSystemPasswordChar = false;
                rePassTxtBox.UseSystemPasswordChar = false;
            }
            else
            {
                passTxtBox.UseSystemPasswordChar = true;
                rePassTxtBox.UseSystemPasswordChar = true;
            }
        }

        private void registerBtn_Click(object sender, EventArgs e)
        {
            
        }

        private void Role_SelectedIndexChanged(object sender, EventArgs e)
        {
            

        }

        private void axxountTxtBox_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
