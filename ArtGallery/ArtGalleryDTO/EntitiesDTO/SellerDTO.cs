﻿using ArtGalleryDTO.InterfacesDTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace ArtGalleryDTO.EntitiesDTO
{
    public class SellerDTO : IBaseEntityDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Account { get; set; }

        public ICollection<ArtObjectDTO> artObjects { get; set; }
    }
}
