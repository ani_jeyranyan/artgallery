﻿using ArtGalleryBLL.InterfacesBLL;
using ArtGalleryDAL;
using ArtGalleryDAL.Entities;
using ArtGalleryDTO.EntitiesDTO;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ArtGalleryBLL.ServicesBLL
{
    public class ShoppingCartService : ServiceBase<ShoppingCart, ShoppingCartDTO>, IShoppingCartService
    {


        public ShoppingCartService(IMapper mapper, ArtGalleryContext context) : base(mapper, context)
        {

        }

    }
}
