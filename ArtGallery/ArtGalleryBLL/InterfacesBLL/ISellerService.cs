﻿using System;
using System.Collections.Generic;
using ArtGalleryDTO.EntitiesDTO;
using ArtGalleryDAL.Entities;
using System.Text;

namespace ArtGalleryBLL.InterfacesBLL
{
    public interface ISellerService : IBaseService<Seller, SellerDTO>
    {

    }
}
