﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ArtGalleryDAL.Entities;
using ArtGalleryDTO.EntitiesDTO;

namespace ArtGalleryBLL.InterfacesBLL
{
    public interface IArtObjectService : IBaseService<ArtObject, ArtObjectDTO>
    {
        Task<ArtObjectDTO> BuyObject(int id);
    }
}
