﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ArtGalleryBLL.InterfacesBLL;
using ArtGalleryDTO.EntitiesDTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ArtGallery.Controllers
{
    [Route("api/seller")]
    [ApiController]
    public class SellerController : ControllerBase
    {
        private ISellerService sellerService;
        public SellerController(ISellerService sellerService)
        {
            this.sellerService = sellerService;
        }
        // GET: api/seller
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var data = await sellerService.GetAll();
                return Ok(data);
            }
            catch(Exception ex)
            {
                return BadRequest(ex);
            }
        }

        // GET: api/seller/5
        [HttpGet("{id}", Name = "Get")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var data = await sellerService.GetById(id);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest("Incorrect id in the Path");
            }
        }

        // POST: api/seller
        [HttpPost]
        public async Task<IActionResult> Post(SellerDTO value)
        {

            try
            {
                var data = await sellerService.Add(value);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        // PUT: api/seller/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] SellerDTO value)
        {
            if (id == value.Id)
            {
                await sellerService.Update(value);
                return Ok("DONE!");
            }
            return BadRequest(id);
        }

        // DELETE: api/seller/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var data = await sellerService.GetById(id);
            if (data == null)
            {
                return BadRequest("Bad Request!");
            }
            else
            {
                await sellerService.Remove(id);
                return Ok("Removed successfully!");
            }
        }
    }
}
