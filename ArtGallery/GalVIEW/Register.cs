﻿using ArtGallery.Controllers;
using ArtGalleryDTO.EntitiesDTO;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GalVIEW
{
    public partial class Register : Form
    {
        public Register()
        {
            InitializeComponent();
        }

        private async void button1_ClickAsync(object sender, EventArgs e)
        {
            SellerDTO seller = new SellerDTO();
            seller.Name = textBox1.Text;
            seller.Account = textBox2.Text;
            seller.Address = textBox3.Text;

            var jsonString = JsonConvert.SerializeObject(seller);
            var content = new StringContent(jsonString);
         
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost:44350/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            try
            {
                var url = await client.PostAsync("api/seller", content);
                return ;
            }
            catch (Exception ex)
            {
                textBox4.Text = ex.Message;
            }

        }


    }
 }

