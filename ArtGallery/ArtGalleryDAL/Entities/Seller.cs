﻿using ArtGalleryDAL.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ArtGalleryDAL.Entities
{
    public class Seller : IBaseEntity
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Account { get; set; }
        
        public ICollection<ArtObject> artObjects { get; set; }
    }
}
