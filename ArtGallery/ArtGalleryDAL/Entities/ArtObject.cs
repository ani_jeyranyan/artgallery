﻿using System;
using Enums;
using ArtGalleryDAL.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ArtGalleryDAL.Entities
{
    public class ArtObject : IBaseEntity
    {
        [Key]
        public int Id { get; set; }
        public string name { get; set; }
        public string artist { get; set; }
        public Types type { get; set; }
        public string description { get; set; }
        public DateTime time { get; set; }
        public decimal price { get; set; }
        public int sellerId { get; set; }

        public Seller seller { get; set; }
        public ICollection<ShoppingCart> shoppingCart { get; set; }

    }
}
