﻿using Microsoft.EntityFrameworkCore;
using ArtGalleryDAL.Entities;
namespace ArtGalleryDAL
{
    public class ArtGalleryContext : DbContext
    {

        public ArtGalleryContext(DbContextOptions<ArtGalleryContext> options) : base(options)
        {

            Database.Migrate();
        }

        public DbSet<Seller> Sellers { get; set; }
        public DbSet<Buyer> Buyers { get; set; }
        public DbSet<ShoppingCart> ShoppingCarts { get; set; }
        public DbSet<ArtObject> Objects { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Seller>()
                 .HasKey(p => p.Id);
            modelBuilder.Entity<Seller>()
                .Property(f => f.Id)
                .ValueGeneratedOnAdd();

            modelBuilder.Entity<Buyer>()
                .HasKey(p => p.Id);
            modelBuilder.Entity<Buyer>()
                .Property(f => f.Id)
                .ValueGeneratedOnAdd();

            modelBuilder.Entity<ArtObject>()
                .HasKey(p => p.Id);
            modelBuilder.Entity<ArtObject>()
                .Property(f => f.Id)
                .ValueGeneratedOnAdd();
            modelBuilder.Entity<ArtObject>()
               .HasOne(t => t.seller)
               .WithMany(p => p.artObjects)
               .HasForeignKey(p => p.sellerId)
               .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<ArtObject>()
                .Property(p => p.sellerId);

            modelBuilder.Entity<ShoppingCart>()
               .HasKey(p => p.Id);
            modelBuilder.Entity<ShoppingCart>()
               .Property(f => f.Id)
               .ValueGeneratedOnAdd();
            modelBuilder.Entity<ShoppingCart>()
               .HasOne(p => p.artObject)
               .WithMany(p => p.shoppingCart)
               .HasForeignKey(p => p.artObjectId)
               .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<ShoppingCart>()
               .HasOne(p => p.buyer)
               .WithMany(p => p.shoppingCart)
               .HasForeignKey(p => p.buyerId)
               .OnDelete(DeleteBehavior.Cascade);
        }
    }
}