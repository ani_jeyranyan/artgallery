﻿using System;
using AutoMapperLib;
using ArtGalleryDAL.Entities;
using ArtGalleryDTO.EntitiesDTO;
using Enums;
using System.Collections.Generic;
using AutoMapper;
using ArtGalleryDAL;

namespace Tests
{
    class Program
    {
        static void Main(string[] args)
        {
           
            ArtObjectDTO obj = new ArtObjectDTO();
            obj.name = "painting";
            obj.Id = 1;
            obj.description = "dghsnkjx";
            obj.artist = "es";
            obj.type = Types.Painting;
            List<ArtObjectDTO> list = new List<ArtObjectDTO>();
            list.Add(obj);
            var config = new MapperConfiguration(cfg => {

                cfg.CreateMap<ArtObject, ArtObjectDTO>()
                //.ForMember(s => s., opt => opt.MapFrom(src => src.user.Id))
                .ReverseMap();
                //.ForPath(s => s.user, opt => opt.Ignore()); ;
            });
            //config.AssertConfigurationIsValid();

            var mapper = config.CreateMapper();
            var dest = mapper.Map<ArtObjectDTO, ArtObject>(obj);
            Console.ReadLine();
           
        }
    }
}
